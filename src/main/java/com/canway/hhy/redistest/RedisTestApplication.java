package com.canway.hhy.redistest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RedisTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(RedisTestApplication.class, args);
        private static final String HELLO = "Hello";	// OK
        private String inttom = "stringTomNametest"; 	// BAD
        // OK
        String messageOk = "message";
        // BAD
        String gomessageBad = "bbq";
    }

}
