package com.canway.hhy.redistest;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.BoundHashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @author hans
 * @PackageName:com.canway.hhy.redistest
 * @ClassName:MyConfigRedisTemplateTest
 * @Description:
 * @date 2021/9/2 8:41
 */


@SpringBootTest
public class MyConfigRedisTemplateTest {

    @Autowired
    private RedisTemplate redisTemplate; //在MyRedisConfig文件中配置了redisTemplate的序列化之后， 客户端也能正确显示键值对了

    @Test
    public void test(){
        User usertest = new User(1,"hhy",18);
        redisTemplate.opsForValue().set("name", usertest);
        System.out.println(redisTemplate.opsForValue().get("name"));
        Map<String, Object> map = new HashMap<>();
        for (int i=0; i<10; i++){
            User user = new User();
            user.setId(i);
            user.setName(String.format("hhy%d", i));
            user.setAge(i+10);
            map.put(String.valueOf(i),user);
        }
        redisTemplate.opsForHash().putAll("test", map);
        BoundHashOperations hashOps = redisTemplate.boundHashOps("test");
        Map map1 = hashOps.entries();
        System.out.println(map1);
    }
    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    static class User implements Serializable {
        private int id;
        private String name;
        private long age;

    }
}